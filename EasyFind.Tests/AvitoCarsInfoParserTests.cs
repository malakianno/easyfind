﻿using System.IO;
using System.Linq;
using NUnit.Framework;
using EasyFind.Parsing.DataReading.Avito.Parsers;

namespace EasyFind.Tests
{
    [TestFixture]
    class AvitoCarsInfoParserTests : BaseTests
    {
        [Test]
        public void Check_Cars_List_Parser_Success()
        {
            var html = File.ReadAllText(Path.Combine(FilesDirectory, "cars-info.html"));
            var parser = new CarsInfoParser();
            var result = parser.Parse(html);
            Assert.IsTrue(result.CarsModels.Any(m => m.Id == 1220 && m.Name == "ВАЗ (LADA)"));
        }
    }
}
