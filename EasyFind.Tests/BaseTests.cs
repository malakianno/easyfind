﻿using System.IO;
using System.Reflection;

namespace EasyFind.Tests
{
    class BaseTests
    {
        protected readonly string WorkingDirectory;
        protected readonly string FilesDirectory;

        public BaseTests()
        {
            var assembly = Assembly.GetExecutingAssembly();
            WorkingDirectory = Path.GetDirectoryName(assembly.Location);
            FilesDirectory = Path.Combine(WorkingDirectory, "Files");
        }
    }
}
