﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace EasyFind.Common.Helpers
{
    public static class EnumHelper
    {
        public static string GetDescription<T>(this T enumerationValue) where T : struct
        {
            var type = enumerationValue.GetType();
            if (!type.IsEnum)
            {
                throw new ArgumentException("EnumerationValue must be of Enum type", nameof(enumerationValue));
            }

            string result = null;
            var memberInfo = type.GetMember(enumerationValue.ToString());
            if (memberInfo.Length > 0)
            {
                var attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attrs.Length != 0)
                {
                    result = ((DescriptionAttribute)attrs[0]).Description;
                }
            }
            return result ?? enumerationValue.ToString();
        }

        public static IDictionary<T, string> GetValues<T>() where T : struct
        {
            return Enum.GetValues(typeof(T)).Cast<T>().ToDictionary(x => x, x => GetDescription<T>(x));
        }
    }
}
