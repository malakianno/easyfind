﻿using System.ComponentModel;

namespace EasyFind.Common.Models.Enums
{
    public enum TransmissionsEnum
    {
        [Description("МКПП")]
        Manual = 1,
        [Description("АКПП")]
        Automatic = 2,
        [Description("Вариатор")]
        CVT = 3,
        [Description("Роботизированная КПП")]
        SemiAutomatic = 4,
    }
}
