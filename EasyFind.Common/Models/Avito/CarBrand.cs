﻿namespace EasyFind.Common.Models.Avito
{
    public class CarBrand
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
