﻿using System;
using System.Collections.Generic;

namespace EasyFind.Common.Models.Avito
{
    public class AdsModel
    {
        public IEnumerable<Ad> Ads { get; set; }
        public DateTime Time { get; set; }
    }
}
