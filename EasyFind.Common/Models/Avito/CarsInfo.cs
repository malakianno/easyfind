﻿namespace EasyFind.Common.Models.Avito
{
    using System.Collections.Generic;

    public class CarsInfo
    {
        public IEnumerable<CarBrand> CarsModels { get; set; }
    }
}
