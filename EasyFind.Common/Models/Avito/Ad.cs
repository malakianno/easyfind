﻿using EasyFind.Common.Models.Enums;

namespace EasyFind.Common.Models.Avito
{
    public class Ad
    {
        public string Id { get; set; }
        public int BrandId { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public bool IsBroken { get; set; }
        public TransmissionsEnum Transmission { get; set; }
        public string Href { get; set; }
    }
}
