﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace EasyFind.DAL
{
    public class EasyFindConnection : Connection, IDisposable
    {
        private static string defaultConnectionString;
        public override string DefaultConnectionString
        {
            get
            {
                return defaultConnectionString;
            }
        }

        public static void SetDefaultConnectionString(string connectionString)
        {
            defaultConnectionString = connectionString;
        }

        public override IDbConnection Create(string connectionString)
        {
            return new SqlConnection(connectionString);
        }

        public void Dispose()
        {
        }
    }
}
