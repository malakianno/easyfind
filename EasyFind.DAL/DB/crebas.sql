/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     04.11.2016 21:05:33                          */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('AvitoTasks') and o.name = 'FK_AvitoTasks_UserId_UsersInfo_Id')
alter table AvitoTasks
   drop constraint FK_AvitoTasks_UserId_UsersInfo_Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('AvitoCarsBrands')
            and   type = 'U')
   drop table AvitoCarsBrands
go

if exists (select 1
            from  sysobjects
           where  id = object_id('AvitoTasks')
            and   type = 'U')
   drop table AvitoTasks
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UsersInfo')
            and   type = 'U')
   drop table UsersInfo
go

/*==============================================================*/
/* Table: AvitoCarsBrands                                       */
/*==============================================================*/
create table AvitoCarsBrands (
   Id                   int                  not null,
   Name                 varchar(50)          not null,
   constraint PK_AVITOCARSBRANDS primary key (Id)
)
go

/*==============================================================*/
/* Table: AvitoTasks                                            */
/*==============================================================*/
create table AvitoTasks (
   Id                   int                  identity,
   Name                 varchar(100)         null,
   Description          varchar(300)         null,
   IsActive             bit                  not null,
   UserId               int                  not null,
   BrandId              int                  not null,
   ModelId              int                  null,
   StartPrice           int                  null,
   EndPrice             int                  null,
   Transmission         int                  null,
   AllowBroken          bit                  null,
   constraint PK_AVITOTASKS primary key (Id)
)
go

/*==============================================================*/
/* Table: UsersInfo                                             */
/*==============================================================*/
create table UsersInfo (
   Id                   int                  identity,
   UserIdentityId       varchar(128)         not null,
   constraint PK_USERSINFO primary key (Id),
   constraint AK_KEY_2_USERSINF unique (UserIdentityId)
)
go

alter table AvitoTasks
   add constraint FK_AvitoTasks_UserId_UsersInfo_Id foreign key (UserId)
      references UsersInfo (Id)
         on update cascade on delete cascade
go

