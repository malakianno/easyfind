﻿namespace EasyFind.DAL.Entities.Avito
{
    public class UserInfoEntity
    {
        public int Id { get; set; }
        public string UserIdentityId { get; set; }
    }
}
