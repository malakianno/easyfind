﻿using EasyFind.Common.Models.Enums;

namespace EasyFind.DAL.Entities.Avito
{
    public class UserTaskEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public bool IsActive { get; set; }
        public int? BrandId { get; set; }
        public int? ModelId { get; set; }
        public int? StartPrice { get; set; }
        public int? EndPrice { get; set; }
        public TransmissionsEnum Transmission { get; set; }
        public bool AllowBroken { get; set; }
    }
}
