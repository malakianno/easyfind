﻿namespace EasyFind.DAL.Dto.Avito
{
    public class UserTaskDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public bool IsActive { get; set; }
        public int? BrandId { get; set; }
        public string BrandName { get; set; }
        public int? ModelId { get; set; }
        public string ModelName { get; set; }
        public int? StartPrice { get; set; }
        public int? EndPrice { get; set; }
        public int? Transmission { get; set; }
        public bool AllowBroken { get; set; }
    }
}
