﻿using System.Data;

namespace EasyFind.DAL
{
    public abstract class Connection
    {
        public abstract string DefaultConnectionString { get; }
        abstract public IDbConnection Create(string connectionString);
        virtual public IDbConnection Create()
        {
            return Create(DefaultConnectionString);
        }
    }
}
