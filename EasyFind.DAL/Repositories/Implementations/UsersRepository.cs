﻿using System.Linq;
using Dapper;
using EasyFind.DAL.Repositories.Interfaces;
using EasyFind.DAL.Entities.Avito;

namespace EasyFind.DAL.Repositories.Implementations
{
    public class UsersRepository : BaseRepository, IUsersRepository
    {
        public void AddUserInfo(UserInfoEntity user)
        {
            using (var connection = OpenConnection())
            {
                const string sql = @"
				    INSERT INTO
                        UsersInfo
                        (
                            UserIdentityId
                        )
				    VALUES
                        (
                            @UserIdentityId
                        )
			    ";
                connection.Execute(sql, user);
            }
        }

        public UserInfoEntity GetUser(string userIdentityId)
        {
            using (var connection = OpenConnection())
            {
                const string sql = @"
				    SELECT
                        Id
                        ,UserIdentityId
                    FROM
                        UsersInfo
                    WHERE
                        UserIdentityId = @userIdentityId
			    ";
               return connection.Query<UserInfoEntity>(sql, new { userIdentityId }).SingleOrDefault();
            }
        }
    }
}
