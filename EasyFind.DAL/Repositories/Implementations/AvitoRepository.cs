﻿using System.Collections.Generic;
using Dapper;
using EasyFind.Common.Models.Avito;
using EasyFind.DAL.Dto.Avito;
using EasyFind.DAL.Entities.Avito;
using EasyFind.DAL.Repositories.Interfaces;

namespace EasyFind.DAL.Repositories.Implementations
{
    public class AvitoRepository : BaseRepository, IAvitoRepository
    {
        public IEnumerable<CarBrand> GetCarsBrands()
        {
            using (var connection = OpenConnection())
            {
                const string sql = @"
				    SELECT
                        Id
                        ,Name
                    FROM
                        AvitoCarsBrands
                    ORDER BY Name
			    ";
                return connection.Query<CarBrand>(sql);
            }
        }

        public void UpdateCarsInfo(CarsInfo carsInfo)
        {
            using (var connection = OpenConnection())
            {
                using (var transaction = connection.BeginTransaction())
                {
                    const string sqlDelete = @"
                        delete
                            from AvitoCarsBrands
                    ";
                    const string sql = @"
				        insert
                            into AvitoCarsBrands(Id, Name)
				        values (@Id, @Name)
			        ";
                    connection.Execute(sqlDelete, null, transaction);
                    connection.Execute(sql, carsInfo.CarsModels, transaction);
                    transaction.Commit();
                }
            }
        }

        public IEnumerable<UserTaskDto> GetTasks(int userId)
        {
            using (var connection = OpenConnection())
            {
                const string sql = @"
				    SELECT
                        at.Id
                        ,at.Name
                        ,at.Description
                        ,at.UserId
                        ,at.IsActive
                        ,at.BrandId
                        ,at.ModelId
                        ,at.StartPrice
                        ,at.EndPrice
                        ,at.Transmission
                        ,at.AllowBroken
                        ,acb.Name as BrandName
				    FROM
                        AvitoTasks at
                        INNER JOIN AvitoCarsBrands acb on acb.Id = at.BrandId
                    WHERE
                        at.UserId = @userId
			    ";
                return connection.Query<UserTaskDto>(sql, new { userId });
            }
        }

        public void CreateTask(UserTaskEntity task)
        {
            using (var connection = OpenConnection())
            {
                const string sql = @"
				    insert into
                        AvitoTasks(
                            Name
                            ,Description
                            ,UserId
                            ,IsActive
                            ,BrandId
                            ,ModelId
                            ,StartPrice
                            ,EndPrice
                            ,Transmission
                            ,AllowBroken
                    )
				    values
                        (
                            @Name
                            ,@Description
                            ,@UserId
                            ,@IsActive
                            ,@BrandId
                            ,@ModelId
                            ,@StartPrice
                            ,@EndPrice
                            ,@Transmission
                            ,@AllowBroken
                        )
			    ";
                connection.Execute(sql, task);
            }
        }

        public void RemoveTask(int id)
        {
            using (var connection = OpenConnection())
            {
                const string sql = @"
				    DELETE FROM
                        AvitoTasks
                    WHERE
                        Id = @Id
			    ";
                connection.Execute(sql, new { Id = id });
            }
        }

        public void RemoveTask(int id, int userId)
        {
            using (var connection = OpenConnection())
            {
                const string sql = @"
				    DELETE FROM
                        AvitoTasks
                    WHERE
                        Id = @Id
                        AND UserId = @UserId
			    ";
                connection.Execute(sql, new { Id = id, UserId = userId });
            }
        }
    }
}
