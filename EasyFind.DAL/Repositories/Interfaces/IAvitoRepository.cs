﻿namespace EasyFind.DAL.Repositories.Interfaces
{
    using Common.Models.Avito;
    using Dto.Avito;
    using Entities.Avito;
    using System.Collections.Generic;

    public interface IAvitoRepository
    {
        IEnumerable<CarBrand> GetCarsBrands();
        void UpdateCarsInfo(CarsInfo carsInfo);
        IEnumerable<UserTaskDto> GetTasks(int userId);
        void CreateTask(UserTaskEntity task);
        void RemoveTask(int id);
        void RemoveTask(int id, int userId);
    }
}
