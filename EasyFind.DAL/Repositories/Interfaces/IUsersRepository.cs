﻿using EasyFind.DAL.Entities.Avito;

namespace EasyFind.DAL.Repositories.Interfaces
{
    public interface IUsersRepository
    {
        void AddUserInfo(UserInfoEntity user);
        UserInfoEntity GetUser(string userIdentityId);
    }
}
