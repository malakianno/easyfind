﻿namespace EasyFind.DAL
{
    using System.Data;

    public class BaseRepository
    {
        private IDbConnection connection;

        protected IDbConnection OpenConnection()
        {
            connection = new EasyFindConnection().Create();
            connection.Open();
            return connection;
        }
    }
}
