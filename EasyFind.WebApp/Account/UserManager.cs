﻿using EasyFind.DAL.Entities.Avito;
using EasyFind.DAL.Repositories.Implementations;
using EasyFind.DAL.Repositories.Interfaces;

namespace EasyFind.WebApp.Account
{
    public static class UserManager
    {
        private static IUsersRepository _usersRepository;

        static UserManager()
        {
            _usersRepository = new UsersRepository();
        }

        public static void AddUserInfo(string userIdentityId)
        {
            var newUser = new UserInfoEntity()
            {
                UserIdentityId = userIdentityId
            };
            _usersRepository.AddUserInfo(newUser);
        }

        public static int GetUserId(string userIdentityId)
        {
            return _usersRepository.GetUser(userIdentityId).Id;
        }

        public static UserInfo GetUserInfo(string userIdentityId)
        {
            var entity = _usersRepository.GetUser(userIdentityId);
            return new UserInfo(entity);
        }
    }
}