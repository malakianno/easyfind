﻿using System;
using EasyFind.DAL.Entities.Avito;

namespace EasyFind.WebApp.Account
{
    public class UserInfo
    {
        public int Id { get; set; }
        public string UserIdentityId { get; set; }

        public DateTime AddedToCache { get; set; }

        public UserInfo(UserInfoEntity entity)
        {
            Id = entity.Id;
            UserIdentityId = entity.UserIdentityId;
        }
    }
}