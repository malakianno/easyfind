﻿namespace EasyFind.WebApp
{
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Configuration;
    using DAL;
    using EasyFind.Logic.Scheduling;

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            EasyFindConnection.SetDefaultConnectionString(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            InitializeSchedule();
    }

        private static void InitializeSchedule()
        {
            var scheduleInitializer = new ScheduleInitializer();
            scheduleInitializer.Initialize();
        }
    }
}
