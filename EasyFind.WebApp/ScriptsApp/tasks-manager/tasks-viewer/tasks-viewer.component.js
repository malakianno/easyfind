﻿;
(function (angular) {
    angular
        .module("tasksManager")
        .component("tasksViewer", {
            templateUrl: "/ScriptsApp/tasks-manager/tasks-viewer/tasks-viewer.template.html",
            bindings: {
                sites: "="
            },
            controllerAs: "vm",
            controller: ["$scope", "tasksManagerService", "tasksManagerAvitoService", tasksViewerController]
        });

    function tasksViewerController($scope, service, avitoService) {
        var vm = this;

        function initialize() {
            vm.tasks = {};
            $scope.$on('updateTasks', function (event, args) {
                switch (args.site) {
                    case vm.sites.avito:
                        vm.tasks.avito = args.tasks;
                        break;
                }
            });

            vm.remove = function (id, site) {
                switch (site) {
                    case vm.sites.avito:
                        avitoService.removeTask(id).then(function (response) {
                            vm.tasks.avito = response.data;
                        });
                        break;
                }
            }
        }

        function activate() {
            avitoService.getTasks().then(function (data) {
                vm.tasks.avito = data.data;
            });
        }

        initialize();
        activate();
    }
})(window.angular)