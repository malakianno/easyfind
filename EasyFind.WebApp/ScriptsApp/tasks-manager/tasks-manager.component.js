﻿;
(function (angular) {
    angular
        .module("tasksManager")
        .component("tasksManager", {
            templateUrl: "/ScriptsApp/tasks-manager/tasks-manager.template.html",
            bindings: {
                params: "="
            },
            controllerAs: "vm",
            controller: ["$scope", "tasksManagerService", "tasksManagerAvitoService", controller]
        });

    function controller($scope, service, avitoService) {
        var vm = this;

        function initialize() {
            vm.sites = {};
            vm.CreateTask = function () {
                switch (vm.userData.ui.site) {
                    case vm.sites.avito:
                        createAvitoTask();
                        break;
                }
            }
        }

        function activate() {
            vm.data = vm.params;
            vm.sites.avito = vm.data.SitesNames[0];
        }

        function createAvitoTask() {
            var data = {
                BrandId: vm.userData.ui.avito.brandId
            }
            avitoService.createTask(data).then(function (data) {
                $scope.$broadcast("updateTasks", {
                    tasks: data.data,
                    site: vm.sites.avito
                });
            });
        }

        initialize();
        activate();
    }
}(window.angular))