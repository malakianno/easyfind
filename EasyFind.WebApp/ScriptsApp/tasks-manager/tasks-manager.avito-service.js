﻿;
(function (angular) {
    angular
        .module("tasksManager")
        .service("tasksManagerAvitoService", ["$http", service]);

    function service($http) {
        var getTasks = function () {
            return $http.get("/api/avito/tasks");
        }
        var createTask = function (data) {
            return $http.post("/api/avito/tasks", data);
        }
        var removeTask = function (id) {
            return $http.delete("/api/avito/tasks/" + id);
        }
        return {
            getTasks: getTasks,
            createTask: createTask,
            removeTask: removeTask
        }
    }
})(window.angular)