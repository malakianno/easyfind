﻿using System.Web.Optimization;

namespace EasyFind.WebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region angular
            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/Scripts/angular/angular.min.js",
                "~/Scripts/angular/angular-animate.min.js",
                "~/Scripts/angular/angular-aria.min.js",
                "~/Scripts/angular/angular-messages.min.js",
                "~/Scripts/angular/angular-material.min.js"
                ));

            bundles.Add(new StyleBundle("~/Content/angular").Include(
                "~/Content/angular/angular-material.min.css",
                "~/Content/angular/iconfont/material-icons.css"
                ));
            #endregion

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap/bootstrap.css",
                      "~/Content/layout.css"));
            #region tasks-manager
            bundles.Add(new ScriptBundle("~/bundles/tasks-manager").Include(
                "~/ScriptsApp/tasks-manager/tasks-manager.module.js",
                "~/ScriptsApp/tasks-manager/tasks-manager.service.js",
                "~/ScriptsApp/tasks-manager/tasks-manager.avito-service.js",
                "~/ScriptsApp/tasks-manager/tasks-manager.component.js",
                "~/ScriptsApp/tasks-manager/tasks-viewer/tasks-viewer.component.js"
                ));

            bundles.Add(new StyleBundle("~/Content/tasks-manager").Include(
                "~/ScriptsApp/tasks-manager/tasks-manager.css"
                ));
            #endregion
        }
    }
}
