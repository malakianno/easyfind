﻿using System.Collections.Generic;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using EasyFind.DAL.Dto.Avito;
using EasyFind.DAL.Entities.Avito;
using EasyFind.Logic.Services.Interfaces;
using EasyFind.Logic.Services.Implementations;

namespace EasyFind.WebApp.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/avito")]
    public class AvitoController : ApiController
    {
        private readonly IAvitoService _avitoService;

        public AvitoController()
        {
            _avitoService = new AvitoService();
        }

        [HttpGet]
        [Route("tasks")]
        public IEnumerable<UserTaskDto> GetTasks()
        {
            return _avitoService.GetTasks(GetUserId());
        }

        [HttpPost]
        [Route("tasks")]
        public IEnumerable<UserTaskDto> CreateTask(UserTaskEntity task)
        {
            var userId = GetUserId();
            task.UserId = userId;
            _avitoService.CreateTask(task);
            return _avitoService.GetTasks(userId);
        }

        [HttpDelete]
        [Route("tasks/{id:int}")]
        public IEnumerable<UserTaskDto> Delete(int id)
        {
            var userId = GetUserId();
            _avitoService.RemoveTask(id, userId);
            return _avitoService.GetTasks(userId);
        }

        private int GetUserId()
        {
            var identityUserId = User.Identity.GetUserId();
            return Account.UserManager.GetUserId(identityUserId);
        }
    }
}
