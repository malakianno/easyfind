﻿using System;
using System.Web.Mvc;
using EasyFind.WebApp.Models;
using EasyFind.Common.Models.Enums;
using EasyFind.Logic.Services.Interfaces;
using EasyFind.Logic.Services.Implementations;

namespace EasyFind.WebApp.Controllers
{
    [Authorize]
    public class TaskController : Controller
    {
        private readonly IAvitoService _avitoService;

        public TaskController()
        {
            _avitoService = new AvitoService();
        }

        public ActionResult Index()
        {
            var model = new SearchCarModel()
            {
                AvitoCars = _avitoService.GetCarsBrands(),
                SitesNames = Enum.GetNames(typeof(SitesEnum))
            };
            return View(model);
        }
    }
}