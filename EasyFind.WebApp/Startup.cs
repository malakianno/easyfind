﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EasyFind.WebApp.Startup))]
namespace EasyFind.WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
