﻿using System.Collections.Generic;
using EasyFind.Common.Models.Avito;

namespace EasyFind.WebApp.Models
{
    public class SearchCarModel
    {
        public IEnumerable<CarBrand> AvitoCars { get; set; }
        public IEnumerable<string> SitesNames { get; set; }
    }
}