﻿using System.Collections.Generic;
using EasyFind.Common.Models.Avito;
using EasyFind.DAL.Dto.Avito;
using EasyFind.DAL.Entities.Avito;

namespace EasyFind.Logic.Services.Interfaces
{
    public interface IAvitoService
    {
        /// <summary>
        /// Получение информации об автомобилях
        /// </summary>
        /// <returns>Информация об автомобилях</returns>
        IEnumerable<CarBrand> GetCarsBrands();

        /// <summary>
        /// Обновляет информацию об автомобилях
        /// </summary>
        /// <param name="carsInfo">Информация об автомобилях</param>s
        void UpdateCarsInfo(CarsInfo carsInfo);

        /// <summary>
        /// Создает поисковую задачу
        /// </summary>
        /// <param name="task">Payload задачи</param>
        void CreateTask(UserTaskEntity task);
        
        /// <summary>
        /// Получение задач пользователя
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <returns>Задачи пользователя</returns>
        IEnumerable<UserTaskDto> GetTasks(int userId);

        /// <summary>
        /// Удаляет задачу
        /// </summary>
        /// <param name="id">Идентификатор задачи</param>
        void RemoveTask(int id);

        /// <summary>
        /// Удаляет задачу
        /// </summary>
        /// <param name="id">Идентификатор задачи</param>
        /// <param name="userId">Идентификатор пользователя</param>
        void RemoveTask(int id, int userId);
    }
}
