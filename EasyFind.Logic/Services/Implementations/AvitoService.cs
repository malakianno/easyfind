﻿using System.Collections.Generic;
using EasyFind.Common.Models.Avito;
using EasyFind.DAL.Repositories.Interfaces;
using EasyFind.DAL.Repositories.Implementations;
using EasyFind.DAL.Entities.Avito;
using EasyFind.DAL.Dto.Avito;
using EasyFind.Logic.Services.Interfaces;

namespace EasyFind.Logic.Services.Implementations
{
    public class AvitoService : IAvitoService
    {
        private readonly IAvitoRepository _avitoRepository;

        public AvitoService()
        {
            _avitoRepository = new AvitoRepository();
        }

        public IEnumerable<CarBrand> GetCarsBrands()
        {
            return _avitoRepository.GetCarsBrands();
        }

        public void UpdateCarsInfo(CarsInfo carsInfo)
        {
            _avitoRepository.UpdateCarsInfo(carsInfo);
        }

        public void CreateTask(UserTaskEntity task)
        {
            _avitoRepository.CreateTask(task);
        }

        public IEnumerable<UserTaskDto> GetTasks(int userId)
        {
            return _avitoRepository.GetTasks(userId);
        }

        public void RemoveTask(int id)
        {
            _avitoRepository.RemoveTask(id);
        }

        public void RemoveTask(int id, int userId)
        {
            _avitoRepository.RemoveTask(id, userId);
        }
    }
}
