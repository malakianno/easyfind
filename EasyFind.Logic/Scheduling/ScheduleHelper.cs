﻿namespace EasyFind.Logic.Scheduling
{
    using System;
    using Quartz;
    using Quartz.Impl;

    public class ScheduleHelper
    {
        private static ScheduleHelper _instance;
        private readonly ISchedulerFactory _schedulerFactory;
        private readonly IScheduler _scheduler;

        private ScheduleHelper()
        {
            _schedulerFactory = new StdSchedulerFactory();
            _scheduler = _schedulerFactory.GetScheduler();
        }

        public static ScheduleHelper GetInstance()
        {
            return _instance ?? (_instance = new ScheduleHelper());
        }

        public DateTimeOffset ScheduleJob<TJob>(ITrigger trigger) where TJob : class, IJob
        {
            IJobDetail jobDetail = JobBuilder.Create<TJob>().Build();
            return _scheduler.ScheduleJob(jobDetail, trigger);
        }

        public void Start()
        {
            _scheduler.Start();
        }
    }
}
