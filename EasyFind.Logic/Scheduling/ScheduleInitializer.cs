﻿namespace EasyFind.Logic.Scheduling
{
    using Jobs;
    using Quartz;
    using System.Configuration;

    public class ScheduleInitializer
    {
        private readonly ScheduleHelper _scheduleHelper;

        public ScheduleInitializer()
        {
            _scheduleHelper = ScheduleHelper.GetInstance();
        }

        public void Initialize()
        {
            var parseAvitoDataCronString = ConfigurationManager.AppSettings["ParseAvitoDataCronString"];
            _scheduleHelper.ScheduleJob<ParseAvitoDataJob>(TriggerBuilder.Create()
                                        .WithCronSchedule(parseAvitoDataCronString)
                                        .Build());
            var parseAvitoAdsCronString = ConfigurationManager.AppSettings["ParseAvitoAdsCronString"];
            _scheduleHelper.ScheduleJob<AvitoAdsParseJob>(TriggerBuilder.Create()
                                        .WithCronSchedule(parseAvitoAdsCronString)
                                        .Build());
            _scheduleHelper.Start();
        }
    }
}
