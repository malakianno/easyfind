﻿using System;
using NLog;
using Quartz;
using EasyFind.Logic.Services.Interfaces;
using EasyFind.Logic.Services.Implementations;
using EasyFind.Parsing.DataReading.Avito;

namespace EasyFind.Logic.Scheduling.Jobs
{
    public class ParseAvitoDataJob : IJob
    {
        private readonly ILogger _logger;
        private readonly DataReader _dataReader;
        private readonly IAvitoService _avitoService;

        public ParseAvitoDataJob()
        {
            _logger = LogManager.GetCurrentClassLogger();
            _dataReader = new DataReader();
            _avitoService = new AvitoService();
        }

        public async void Execute(IJobExecutionContext context)
        {
            try
            {
                _logger.Info(string.Format("{0} is started", this.GetType()));
                var carsInfo = await _dataReader.Read();
                _avitoService.UpdateCarsInfo(carsInfo);
                _logger.Info(string.Format("{0} is completed successfully", this.GetType()));
            }
            catch (Exception e)
            {
                _logger.Error(e, string.Format("{0} is completed with errors", this.GetType()));
            }
        }
    }
}
