﻿namespace EasyFind.Logic.Scheduling.Jobs
{
    using System.Collections.Generic;
    using NLog;
    using Quartz;
    using Common.Models.Avito;
    using DAL.Repositories.Implementations;
    using DAL.Repositories.Interfaces;
    using Parsing.DataReading.Avito;

    public class AvitoAdsParseJob : IJob
    {
        private readonly ILogger _logger;
        private readonly AdsDataReader _dataReader;
        private readonly IAvitoRepository _repository;

        public AvitoAdsParseJob()
        {
            _logger = LogManager.GetCurrentClassLogger();
            _dataReader = new AdsDataReader();
            _repository = new AvitoRepository();
        }

        public async void Execute(IJobExecutionContext context)
        {
            _logger.Info(string.Format("{0} is started", this.GetType()));
            var adsModel = await _dataReader.Read();
            SetCarsInfo(adsModel.Ads);
            _logger.Info(string.Format("{0} is finished", this.GetType()));
        }

        private void SetCarsInfo(IEnumerable<Ad> ads)
        {
            var carsBrands = _repository.GetCarsBrands();
            foreach (var ad in ads)
            {
                var brandModel = ad.Brand;
                foreach (var brand in carsBrands)
                {
                    if (ad.Brand.StartsWith(brand.Name))
                    {
                        ad.BrandId = brand.Id;
                        ad.Brand = brand.Name;
                        break;
                    }
                }
            }
        }
    }
}
