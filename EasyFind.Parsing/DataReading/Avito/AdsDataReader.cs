﻿using System.Net.Http;
using System.Threading.Tasks;
using EasyFind.Parsing.DataReading.Avito.Parsers;
using EasyFind.Common.Models.Avito;

namespace EasyFind.Parsing.DataReading.Avito
{
    public class AdsDataReader : IDataReader<AdsModel>
    {
        public const string CarsAdsUrl = "https://www.avito.ru/rossiya/avtomobili/s_probegom?view=list";
        private IParser<AdsModel> _adsParser;

        public AdsDataReader()
        {
            _adsParser = new AdsParser();
        }

        public async Task<AdsModel> Read()
        {
            HttpResponseMessage response;
            using (var client = new HttpClient())
            {
                response = await client.GetAsync(CarsAdsUrl);
            }
            var ads = await response.Content.ReadAsStringAsync();
            return _adsParser.Parse(ads);
        }
    }
}
