﻿using System;
using System.Collections.Generic;
using HtmlAgilityPack;
using JetBrains.Annotations;
using EasyFind.Common.Models.Avito;
using EasyFind.Parsing.Helpers;

namespace EasyFind.Parsing.DataReading.Avito.Parsers
{
    public class AdsParser : IParser<AdsModel>
    {
        public AdsModel Parse([NotNull] string html)
        {
            var ads = new List<Ad>();
            var result = new AdsModel()
            {
                Ads = ads
            };
            var document = new HtmlDocument();
            document.LoadHtml(html);
            var documentNode = document.DocumentNode;
            var builder = new XPathBuilder();
            var adsXpath = builder.FindByClass("catalog-list").Start(true).FindByClass("item").ToString();
            var adsNodes = documentNode.SelectNodes(adsXpath);
            foreach(var adNode in adsNodes)
            {
                var ad = new Ad();
                var titleXpath = builder.Start(true).FindByClass("description-title-link", "a").ToString();
                var titleNode = adNode.SelectSingleNode(titleXpath);
                ad.Id = titleNode.Id;
                ad.Brand = titleNode.InnerText;
                ads.Add(ad);
            }
            result.Time = DateTime.Now;
            return result;
        }
    }
}
