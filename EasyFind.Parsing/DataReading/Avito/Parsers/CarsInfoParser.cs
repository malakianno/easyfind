﻿namespace EasyFind.Parsing.DataReading.Avito.Parsers
{
    using System;
    using System.Linq;
    using HtmlAgilityPack;
    using JetBrains.Annotations;
    using Common.Models.Avito;
    using Helpers;

    public class CarsInfoParser : IParser<CarsInfo>
    {
        public CarsInfo Parse([NotNull] string html)
        {
            var result = new CarsInfo();
            var document = new HtmlDocument();
            document.LoadHtml(html);
            var documentNode = document.DocumentNode;
            var builder = new XPathBuilder();
            var optionsXpath = builder.FindByAttribute("id", "param_210", "select").ToString();
            var optionsNode = documentNode.SelectSingleNode(optionsXpath);
            var optionXPath = builder.Start(true).FindByTag("option").ToString();
            var carsNodeCollection = optionsNode.SelectNodes(optionXPath).Where(n => n.Attributes["value"] != null &&
                !n.GetAttributeValue("value", string.Empty).Equals(string.Empty));
            result.CarsModels = carsNodeCollection.Select(x => new CarBrand() { Id = Convert.ToInt32(x.Attributes["value"].Value), Name = x.NextSibling.InnerText });
            return result;
        }
    }
}
