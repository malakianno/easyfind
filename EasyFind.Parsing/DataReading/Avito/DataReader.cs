﻿namespace EasyFind.Parsing.DataReading.Avito
{
    using System.Net.Http;
    using Parsers;
    using Common.Models.Avito;
    using System.Threading.Tasks;
    using Parsing;

    public class DataReader : IDataReader<CarsInfo>
    {
        public const string CarsInfoUrl = "https://m.avito.ru/search/rossiya/avtomobili";
        private IParser<CarsInfo> _carsInfoParser;

        public DataReader()
        {
            _carsInfoParser = new CarsInfoParser();
        }

        public async Task<CarsInfo> Read()
        {
            HttpResponseMessage response;
            using (var client = new HttpClient())
            {
                response = await client.GetAsync(CarsInfoUrl);
            }
            var carsInfo = await response.Content.ReadAsStringAsync();
            return _carsInfoParser.Parse(carsInfo);
        }
    }
}
