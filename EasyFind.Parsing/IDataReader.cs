﻿using System.Threading.Tasks;

namespace EasyFind.Parsing
{
    public interface IDataReader<TModel>
    {
        /// <summary>
        /// Чтение данных
        /// </summary>
        Task<TModel> Read();
    }
}
