﻿namespace EasyFind.Parsing.Helpers
{
    using System.Text;
    using JetBrains.Annotations;
    public class XPathBuilder
    {
        [NotNull]
        private readonly StringBuilder _pathBuilder;

        public XPathBuilder(bool fromCurrentNode = false)
        {
            _pathBuilder = new StringBuilder();
            Start(fromCurrentNode);
        }

        public override string ToString()
        {
            return _pathBuilder.ToString();
        }

        [NotNull]
        public XPathBuilder Start(bool fromCurrentNode = false)
        {
            _pathBuilder.Clear().Append(fromCurrentNode ? ".//" : "//");
            return this;
        }

        [NotNull]
        public XPathBuilder FindByTag(string tagName)
        {
            _pathBuilder.Append(tagName);
            return this;
        }

        [NotNull]
        public XPathBuilder FindByClass(string className, string tagName = "div")
        {
            _pathBuilder.Append(GetSelectorByClassName(tagName, className));
            return this;
        }

        [NotNull]
        public XPathBuilder FindByAttribute(string attributeName, string attributeValue, string tagName = "div")
        {
            _pathBuilder.Append(GetSelectorByAttribute(tagName, attributeName, attributeValue, true));
            return this;
        }

        [NotNull]
        public XPathBuilder FindByText(string text, string tagName = "div", bool searchByLike = false)
        {
            _pathBuilder.Append(GetSelectorByText(tagName, text, searchByLike));
            return this;
        }

        [NotNull]
        public XPathBuilder FindTextNodeByText(string text, bool searchByLike = false)
        {
            _pathBuilder.Append(GetTextNodeSelectorByText(text, searchByLike));
            return this;
        }

        [NotNull]
        public XPathBuilder FindChildrenByTag(string tagName)
        {
            _pathBuilder.Append("/").Append(tagName);
            return this;
        }

        [NotNull]
        public XPathBuilder FindChildrenByClass(string className, string tagName = "div")
        {
            _pathBuilder.Append("/").Append(GetSelectorByClassName(tagName, className));
            return this;
        }

        [NotNull]
        public XPathBuilder FindChildrenByAttribute(string attributeName, string attributeValue, string tagName = "div")
        {
            _pathBuilder.Append("/").Append(GetSelectorByAttribute(tagName, attributeName, attributeValue, true));
            return this;
        }

        [NotNull]
        public XPathBuilder FindChildrenByText(string text, string tagName = "div", bool searchByLike = false)
        {
            _pathBuilder.Append("/").Append(GetSelectorByText(tagName, text, searchByLike));
            return this;
        }

        [NotNull]
        public XPathBuilder FindChildren()
        {
            _pathBuilder.Append("/");
            return this;
        }

        [NotNull]
        public XPathBuilder FindAllChildren()
        {
            _pathBuilder.Append("//");
            return this;
        }

        [NotNull]
        private string GetSelectorByClassName(string tagName, string className)
        {
            return GetSelectorByAttribute(tagName, "class", className, false);
        }

        [NotNull]
        private string GetSelectorByText(string tagName, string text, bool searсhByLike)
        {
            return tagName + (searсhByLike ? $"[contains(normalize-space(text()), '{text}')]" : $"[text() = '{text}']");
        }

        [NotNull]
        private string GetTextNodeSelectorByText(string text, bool searсhByLike)
        {
            return "text()" + (searсhByLike ? $"[contains(normalize-space(.), '{text}')]" : $"[. = '{text}']");
        }

        [NotNull]
        private string GetSelectorByAttribute(string tagName, string attributeName, string attributeValue, bool searсhByLike)
        {
            var space = searсhByLike ? "" : " ";
            return $"{tagName}[contains(concat(' ', normalize-space(@{attributeName}), ' '), '{space}{attributeValue}{space}')]";
        }
    }
}
