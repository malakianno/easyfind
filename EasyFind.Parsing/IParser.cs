﻿namespace EasyFind.Parsing
{
    using JetBrains.Annotations;

    public interface IParser<out TModel>
    {
        [NotNull] TModel Parse([NotNull] string html);
    }
}
